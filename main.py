from tkinter import *
from tkinter import filedialog
from PIL import ImageTk, Image
from tkcolorpicker import askcolor
from service import rgb_to_string
from windows import Show_Window, ColorsHistogramWindow
from image_operations import convert_to_grayscale


class Main_Window:
    def __init__(self, master):
        self.master = master
        self.master.geometry("600x300")
        self.master.title("Menu")

        self.image_path = None
        self.image = None
        self.image_grayscale = None
        self.width = 0
        self.height = 0
        self.btn_load = Button(master, text="Load Image", command=self.load)
        self.btn_load.grid(row=0, column=0)

        self.btn_show = Button(master, text="Show Image", command=self.show_picture)
        self.btn_show.grid(row=1, column=0)
        self.btn_show['state'] = "disabled"




    def load(self):
        self.image_path = filedialog.askopenfilename()
        self.width, self.height = Image.open(self.image_path).size
        self.image = Image.open(self.image_path)
        # self.btn_hist['state'] = "normal"
        self.btn_show['state'] = "normal"


    def show_picture(self):
        window = Show_Window(self, self.image, self.width, self.height)

root = Tk()
main_window = Main_Window(root)

root.mainloop()